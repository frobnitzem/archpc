
- 10.13.13.22 - vm-node-full (64 cores, 480 GBs of RAM, 300 GBs of storage, 4 A100 (40x SXM linked)).
   \  10.13.13.20 - vm-lower (32 cores, 240 GBs of RAM, 300 GBs of storage, 4 A100 (20x SXM linked)).
*     \- 10.13.13.21 - vm-lower-half (16 cores, 120 GBs of RAM, 300 GBs of storage, 2 A100 (20x SXM linked)).
*     \- 10.13.13.23 - vm-node-half (32 cores, 240 GBs of RAM, 300 GBs of storage, 2 A100 (40x SXM linked)).
   \  10.13.13.24 - vm-upper (32 cores, 240 GBs of RAM, 300 GBs of storage, 4 A100 (20x SXM linked)).
      \- 10.13.13.25 - vm-upper-half (16 cores, 120 GBs of RAM, 300 GBs of storage, 2 A100 (20x SXM linked)).
*        \-   10.13.13.26 - vm-upper-q1 (16 cores, 120 GBs of RAM, 300 GBs of storage, 4 A100 (10x SXM linked)).
*        \-   10.13.13.27 - vm-upper-q2 (16 cores, 120 GBs of RAM, 300 GBs of storage, 4 A100 (10x SXM linked)).
