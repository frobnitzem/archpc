#!/bin/bash

RANKS=$1
GPU_COUNT=$2
MPIRUN=/home/ornl/spack/opt/spack/linux-ubuntu22.04-zen2/gcc-11.4.0/openmpi-5.0.3-57mxmnwdbono7ys5n5pq4woatmvk53rs/bin/mpirun
MPI_ARGS="--hostfile hostfile --mca orte_tmpdir_base /tmp/podman-mpirun"
PODMAN_ARGS="--env-host -v /tmp/mpirun:/tmp/mpirun --userns=keep-id --net=host --pid=host --ipc=host"
SING_ARGS="-B /tmp/mpirun"

$MPIRUN $MPI_ARGS -np $RANKS \
   apptainer run $SING_ARGS --nv -B "$PWD:/host_pwd" \
   			      --pwd /host_pwd lammps.sif \
     mpirun -np $RANKS ./lmp.sh ${RANKS}x${GPU_COUNT}
