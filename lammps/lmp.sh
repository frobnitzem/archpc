#!/bin/bash

         #-pk kokkos cuda/aware on neigh full comm device binsize 2.8 \
lmp -k on g 8 \
         -sf kk \
         -pk kokkos neigh full binsize 2.8 \
         -var x 16 \
         -var y 8 \
         -var z 16 \
         -in melt.inp \
         -log "$1.log"
