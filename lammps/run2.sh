#!/bin/bash

RANKS=$1
GPU_COUNT=$2
MPIRUN=/home/ornl/spack/opt/spack/linux-ubuntu22.04-zen2/gcc-11.4.0/openmpi-5.0.3-57mxmnwdbono7ys5n5pq4woatmvk53rs/bin/mpirun

$MPIRUN -np $RANKS \
     ./lmp -k on g $GPU_COUNT \
         -sf kk \
         -pk kokkos cuda/aware on neigh full comm device binsize 2.8 \
         -var x 16 \
         -var y 8 \
         -var z 16 \
         -in melt.inp \
         -log s"$RANKS"x"$GPU_COUNT".log
