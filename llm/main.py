#!/usr/bin/env python3
# https://fgiasson.com/blog/index.php/2023/08/23/how-to-deploy-hugging-face-models-in-a-docker-container/
import time
import torch
from flask import Flask, request, jsonify
from transformers import (
    AutoTokenizer,
    AutoModelForCausalLM,
    LogitsProcessorList,
    MinLengthLogitsProcessor,
    StoppingCriteriaList,
    MaxLengthCriteria,
)

torch.set_default_device("cuda")

def get_model(model_path):
    """Load a Hugging Face model and tokenizer from the specified directory"""
    tokenizer = AutoTokenizer.from_pretrained(model_path,
                        padding_side = "left",
                        trust_remote_code=True)
    #tokenizer.padding_side = "left"
    tokenizer.pad_token = tokenizer.eos_token
    model = AutoModelForCausalLM.from_pretrained(model_path, torch_dtype="auto", trust_remote_code=True)
    return model, tokenizer

# Load the models and tokenizers for each supported language
model, tokenizer = get_model('models/phi-1.5/')
#model, tokenizer = get_model('models/phi-2/')

app = Flask(__name__)

def is_supported(model):
    """Check if the specified model is supported"""
    supported = ['phi-1.5', 'phi-2']
    return model in supported

@app.route('/query/<modelname>/', methods=['POST'])
def query_endpoint(modelname):
    """Query a language model.
    """
    if not is_supported(modelname):
        return jsonify({'error': 'Model not supported'}), 400

    data = request.get_json()
    from_text = data.get('text', '')

    if from_text:
        to_text = tokenizer.decode(model.generate(tokenizer.encode(from_text, return_tensors='pt')).squeeze(), skip_special_tokens=True)

        return jsonify({f'text': to_text})
    else:
        return jsonify({'error': 'Text query not provided'}), 400
 
if __name__ == '__main__':
    import sys
    argv = sys.argv[1:]
    if len(argv) > 1:
        max_iter = int(argv[1])
    else:
        max_iter = 10
    if len(argv) > 2:
        ctxt = int(argv[2])
    else:
        ctxt = 256
    ctxt = min(ctxt, 2048)
    ctxt = max(ctxt, 4)
    #app.run(host='0.0.0.0', port=6000, debug=True)
    from_text = ["Instruct: Is a sentence that refers to itself necessarily false?\nOutput:",
                 "Has COVID vaccination resulted in an increased incidence of heart issues in some segments of the population?\n",
                 "How much faster can GPUs compute with FP16 as opposed to FP32 or FP64?\n",
                 "Can the sun simultaneously explode and implode?\nWhat elements would be released?\n",
                 "What happens when the heat death of the universe occurs?\n",
                 "Is it better to exercise in the morning or the afternoon?\n",
                 "How many cups of coffee should I drink on a Friday?\n",
                 "What would Chuck Norris do with a large language model?\n"]
    #from_text = from_text[0:1]
    inputs = tokenizer(from_text,
                       return_tensors="pt",
                       padding=True)
    tokens = 0
    nonzero = 0
    display = False
    start = time.time()

    # instantiate logits processors
    logits_processor = LogitsProcessorList([
            MinLengthLogitsProcessor(15, eos_token_id=tokenizer.eos_token_id),
        ])
    stopping_criteria = StoppingCriteriaList([
                    MaxLengthCriteria(max_length=ctxt)])

    for i in range(max_iter):
        outputs = model.generate(**inputs,
                               max_new_tokens=ctxt//2,
                               pad_token_id=tokenizer.eos_token_id)
        #outputs = model.sample(**inputs,
        #                       logits_processor=logits_processor,
        #                       stopping_criteria=stopping_criteria,
        #                       pad_token_id=tokenizer.eos_token_id,
        #                      )
        # Do some statistics on the generated output tokens.
        last = inputs["input_ids"].shape[1]
        nnz = (outputs[:,last:] != tokenizer.eos_token_id).sum(1)
        print(nnz)
        nonzero += nnz.sum()

        # Create next inputs
        st = max(outputs.shape[1] - ctxt//2, 0)
        tokens += outputs.shape[0]*( outputs.shape[1] - last )
        inputs  = { "input_ids": outputs[:,st:],
                    "attention_mask": outputs[:,st:] != tokenizer.eos_token_id
                  }

        if display:
            to_text = tokenizer.batch_decode(outputs, skip_special_tokens=True)
            sp = "-"*40 + "\n"
            print(sp + sp.join(to_text))

        #from_text = [t[len(f):] for t,f in zip(to_text, from_text)]
    end = time.time()-start
    print(f"Generated {tokens} tokens in {end} seconds: {tokens/end} tokens/sec.")
    print(f"Percent nonzero = {nonzero/tokens}")
