import os
from transformers import AutoModelForCausalLM, AutoTokenizer

def download_model(model_path, model_name):
    """Download a Hugging Face model and tokenizer to the specified directory"""
    # Check if the directory already exists
    if not os.path.exists(model_path):
        # Create the directory
        os.makedirs(model_path)

    tokenizer = AutoTokenizer.from_pretrained(model_name,
                                              trust_remote_code=True)
    model = AutoModelForCausalLM.from_pretrained(model_name,
                                                 torch_dtype="auto",
                                                 trust_remote_code=True)

    # Save the model and tokenizer to the specified directory
    model.save_pretrained(model_path)
    tokenizer.save_pretrained(model_path)

download_model('models/phi-1.5/', 'microsoft/phi-1.5')
download_model('models/phi-2/', 'microsoft/phi-2')
