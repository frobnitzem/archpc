# Installation process notes

## System Setup

1. Create a toor user with password login disabled
and a `/home/toor/.ssh/authorized_keys`
that includes the user's public key.
Note `/home/toor/.ssh` should have permissions `700`.

    useradd --create-home toor

2. Add to /etc/sudoers:
```
toor    ALL=(ALL:ALL) NOPASSWD:ALL
```

3. Use ansible to bring packages up to date
   and install gfortran-13

       python3 -m venv
       activate venv
       pip install ansible

       ansible-galaxy collection install ansible.posix
       ansible-playbook -i inventory upgrade.yaml

4. use ansible to setup an nfs share on
   /home and /scratch (tmpfs) (see add'l playbooks).

Note: ansible can be installed on a home machine
using `pip install ansible`.

## Software Setup

Copy the llm and lammps sub-directories to
hosts which will be running the applications.

See `inst.sh` in this directory for NVIDIA
container toolkit install steps (runnable).
Additional LAMMPS installation instructions are present
in the full write-up.

See `llm/install.sh` for the (runnable)
script to install the llm.

Once installed on the compute node,
the `launch.sh` script should be copied
to the compute nodes and run to test that
outputs are produced correctly.

