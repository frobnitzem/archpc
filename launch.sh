#!/bin/bash
# Launcher script for running concurrent applications.

if [ $# -ne 5 ]; then
    echo "Usage: $0 <lmp> <lmp_gpu> <llm> <llm_gpu> <logdir>"
    exit 1
fi
LMP=$1
LMP_GPU=$2
LLM=$3
LLM_GPU=$4
logdir=$5

mkdir -p $logdir

out=${LMP}_${LMP_GPU}_${LLM}_${LLM_GPU}
rm $logdir/$out.llm $logdir/$out.lmp

. llm/venv/bin/activate

start=`date +%s`

if [ $LMP -gt 0 ]; then
  cd lammps
  CUDA_VISIBLE_DEVICES=0
  for((i=1; i<$LMP_GPU; i++)); do
    CUDA_VISIBLE_DEVICES=$CUDA_VISIBLE_DEVICES,$i
  done
  export CUDA_VISIBLE_DEVICES
  ./run1.sh $LMP 1.log >../$logdir/$out.lmp.0 &
  cd ..
fi

cd llm
for((i=0; i<$LLM; i++)); do
    export CUDA_VISIBLE_DEVICES=$((LMP_GPU+(i%LLM_GPU)))
    python main.py 20 >../$logdir/$out.llm.$i &
done
cd ..

end=`date +%s`
wait
echo "Completed in $((end-start)) seconds."

echo "lammps:"
x=`awk '/^Loop time of/ {print $4}' $logdir/$out.lmp.0`
y=`awk '/^Performance:/ {print $6}' $logdir/$out.lmp.0`
echo $x $y | tee $logdir/$out.lmp

echo "llm:"
for((i=0; i<$LLM; i++)); do
  awk '/^Generated/ {print $5,$7}' $logdir/$out.llm.$i
done | tee $logdir/$out.llm
